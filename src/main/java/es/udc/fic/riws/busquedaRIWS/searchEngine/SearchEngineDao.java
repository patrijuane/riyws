package es.udc.fic.riws.busquedaRIWS.searchEngine;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.beans.DocumentObjectBinder;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.TermsResponse.Term;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.stereotype.Component;

@Component
public class SearchEngineDao{


	String urlString = "http://localhost:8983/solr/collection1";
	HttpSolrClient solr = new HttpSolrClient.Builder(urlString).build();
	
	public SearchEngineDao() {
		solr.setParser(new XMLResponseParser()); 
	}
	
	public List<VideoGame> queries(String console, String name, String minscore, String maxscore,
			String minuserscore, String maxuserscore, String developer) throws SolrServerException, IOException{
		
		SolrQuery query = new SolrQuery();
		String q = "";
		
		if (console!=null && console.length()!=0) {
			q+="meta_console:"+console;
		}
		if(name!=null && name.length()!=0) {
			if(console!=null && console.length()!=0) {
				q+=" AND ";
			}
			q+="meta_name:"+"*"+name+"*";
		}
		
		String minall="*";
		String maxall="*";
		if(minscore!=null && minscore.length()!=0 && Integer.parseInt(minscore)>0) {
				minall=minscore;
		}
		if(maxscore!=null && maxscore.length()!=0 && Integer.parseInt(maxscore)<100) {
			maxall=maxscore;
		}

			
		if((console!=null && console.length()!=0) || (name!=null && name.length()!=0)) {
			q+=" AND ";
		}
		q+="meta_score:["+minall+" TO "+maxall+"]";
		
		
		String minuserall="*";
		String maxuserall="*";
		if(minuserscore!=null && minuserscore.length()!=0 && Integer.parseInt(minuserscore)>0) {
			minuserall=minuserscore;
		}
		if(maxuserscore!=null && maxuserscore.length()!=0 && Integer.parseInt(maxuserscore)<10) {
			maxuserall=maxuserscore;
		}
		
	/*	if((console!=null && console.length()!=0) || (name!=null && name.length()!=0) ||
			(score!=null && score.length()!=0)) { */
			q+=" AND ";
		q+="meta_userscore:["+minuserall+" TO "+maxuserall+"]";
		
		
		if(developer!=null && developer.length()!=0) {
			System.out.println("\n developer: "+developer);
			/*if((console!=null && console.length()!=0) || (name!=null && name.length()!=0) ||
					(score!=null && score.length()!=0) || (userscore!=null && userscore.length()!=0)) {*/
			q+=" AND ";
			/*}*/
			q+="meta_developer:"+"*"+developer+"*";
		}
		System.out.println(" QUERY q:  "+q);
		query.setQuery(q);
		System.out.println(" Query:  "+query);
		
		query.setStart(0);
		//query.setRows(5);
		
		QueryResponse response = solr.query(query);
		
		// execute the query on the server and get results
		//QueryResponse res = solrServer.query(solrQuery);
		
		SolrDocumentList list = response.getResults();
		 //binding solr to java entitie (google)
		
		DocumentObjectBinder binder = new DocumentObjectBinder();
		
		List<VideoGame> videogames = binder.getBeans(VideoGame.class, list);
		System.out.println("videogames size in DAO: "+videogames.size());

		Iterator<VideoGame> i = videogames.iterator();
		while(i.hasNext()) {
			VideoGame v = i.next();
			if (v.getName()==null) {
				i.remove();
			}
		} 

		return videogames;
		
	} 
	
	
	public List<String> consoles() throws SolrServerException, IOException{
		SolrQuery query = new SolrQuery();
		String q = "meta_console:*";
		System.out.println(" QUERY q:  "+q);
		query.setQuery(q);
		System.out.println(" Query:  "+query);
		query.setStart(0);
		
		query.setFacet(true);
	    query.setFacetMinCount(1);
	    query.addFacetField("meta_console");	
		

		QueryResponse response = solr.query(query);
		
		  List<Count> results = response.getFacetField("meta_console").getValues(); //response.getResults();
			List<String> consoles = new ArrayList();
	        for (Count result: results) {
	        	String console = new String();
	        	console=result.getName();
	        	consoles.add(console);
	        }
	        System.out.println("tamaño lista de consolas"+consoles.size());
	        return consoles;
	}
	

}