package es.udc.fic.riws.busquedaRIWS.searchEngine;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class SearchEngineController {

	//private static final String SEARCHENGINE_VIEW_NAME = "searchEngine/searchEngine";
	
    @Autowired
    private SearchEngineService searchEngineService;
    
    private String console;
    private String name;
    private String minscore;
    private String maxscore;
    private String minuserscore;
    private String maxuserscore;
    private String developer;

    @GetMapping("searchEngine")
    public String searchEngine(Model model, 
    		@RequestParam(name = "console", required = false) String console,
    		@RequestParam(name = "name", required = false) String name,
    		@RequestParam(name = "minscore", required = false) String minscore,
    		@RequestParam(name = "maxscore", required = false) String maxscore,
    		@RequestParam(name = "minuserscore", required = false) String minuserscore,
    		@RequestParam(name = "maxuserscore", required = false) String maxuserscore,
    		@RequestParam(name="developer", required=false) String developer) throws SolrServerException, IOException {
    	
       model.addAttribute(new SearchEngineForm());
    
       List<VideoGame> videogames = searchEngineService.videoGamesCrawled(console,name,minscore,maxscore,
    		   minuserscore,maxuserscore,developer);
      model.addAttribute("videogames",videogames);
      
      
      /*Autocompletado nombres de consolas*/
      List<String> consoles = searchEngineService.consoles();
      System.out.println("\n\n\n\n\n\n lista consolas "+consoles.size());
      model.addAttribute("list", consoles);
      
       return "searchEngine/searchEngine";
    }    
    
    @GetMapping("searchEngine/{page}")
    public String searchEnginePage(Model model, 
    		@RequestParam(name = "console", required = false) String console,
    		@RequestParam(name = "name", required = false) String name,
    		@RequestParam(name = "minscore", required = false) String minscore,
    		@RequestParam(name = "maxscore", required = false) String maxscore,
    		@RequestParam(name = "minuserscore", required = false) String minuserscore,
    		@RequestParam(name = "maxuserscore", required = false) String maxuserscore,
    		@RequestParam(name="developer", required=false) String developer
    		) throws SolrServerException, IOException {
    
    	   model.addAttribute(new SearchEngineForm());
           List<VideoGame> videogames = searchEngineService.videoGamesCrawled(this.console,this.name,this.minscore,this.maxscore,
        		   this.minuserscore,this.maxuserscore,this.developer);

           model.addAttribute("videogames",videogames);
           
           /*Autocompletado nombres de consolas*/
           List<String> consoles = searchEngineService.consoles();

           model.addAttribute("list", consoles);
           return "searchEngine/searchEngine";
    }

    @PostMapping("searchEngine")
    public String searchEngine(Model m, @Valid @ModelAttribute SearchEngineForm searchEngineForm) 
    		throws SolrServerException, IOException {
        m.addAttribute(new SearchEngineForm());
        
        this.console = searchEngineForm.getConsole();
        this.name = searchEngineForm.getName();
        this.minscore = searchEngineForm.getMinscore();
        this.maxscore= searchEngineForm.getMaxscore();
        this.minuserscore = searchEngineForm.getMinuserscore();
        this.maxuserscore = searchEngineForm.getMaxuserscore();
        this.developer = searchEngineForm.getDeveloper();

        List<VideoGame> videogames = searchEngineService.videoGamesCrawled(this.console,this.name,this.minscore,this.maxscore,
        		this.minuserscore,this.maxuserscore,this.developer);
        m.addAttribute("videogames", videogames);
        
        /*Autocompletado nombres de consolas*/
        List<String> consoles = searchEngineService.consoles();

        m.addAttribute("list", consoles);

        return "searchEngine/searchEngine";
    }
    
    @PostMapping("searchEngine/searchEngine")
    public String searchEngineTrasPost(Model m, @Valid @ModelAttribute SearchEngineForm searchEngineForm) 
    		throws SolrServerException, IOException {
        m.addAttribute(new SearchEngineForm());
        
        this.console = searchEngineForm.getConsole();
        this.name = searchEngineForm.getName();
        this.minscore = searchEngineForm.getMinscore();
        this.maxscore= searchEngineForm.getMaxscore();
        this.minuserscore = searchEngineForm.getMinuserscore();
        this.maxuserscore = searchEngineForm.getMaxuserscore();
        this.developer = searchEngineForm.getDeveloper();
              
        List<VideoGame> videogames = searchEngineService.videoGamesCrawled(this.console,this.name,this.minscore,this.maxscore,
        		this.minuserscore,this.maxuserscore,this.developer);
        m.addAttribute("videogames", videogames);
        
        /*Autocompletado nombres de consolas*/
        List<String> consoles = searchEngineService.consoles();

        m.addAttribute("list", consoles);

        return "searchEngine/searchEngine";
    }  
    
    
  /*  @PostMapping("searchEngine/{page}")
    public String searchEngine(Model m, @Valid @ModelAttribute SearchEngineForm searchEngineForm,
    		@PathVariable("page") int pageNum) throws SolrServerException, IOException {
    	
    	m.addAttribute(new SearchEngineForm());
         
        String console = searchEngineForm.getConsole();
        String name = searchEngineForm.getName();
        String minscore = searchEngineForm.getMinscore();
        String maxscore= searchEngineForm.getMaxscore();
        String minuserscore = searchEngineForm.getMinuserscore();
        String maxuserscore = searchEngineForm.getMaxuserscore();
        String developer = searchEngineForm.getDeveloper();
        
        Pageable pageable = new PageRequest(pageNum,5,Sort.Direction.DESC,"name");
        int pagenum = pageable.getPageNumber();
       
        Page<VideoGame> videogames = searchEngineService.videoGamesCrawled(pagenum,console,name,minscore,maxscore,
        		minuserscore,maxuserscore,developer);
        m.addAttribute("videogames", videogames);
       

        return "redirect:/searchEngine";
    } */
    
}
