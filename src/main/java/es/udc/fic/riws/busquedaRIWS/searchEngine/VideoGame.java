package es.udc.fic.riws.busquedaRIWS.searchEngine;

import org.apache.solr.client.solrj.beans.Field;

public class VideoGame{
	
	@Field("id")
	private String id;
	
	@Field("url")
	private String url;

	@Field("meta_console")
	private String console;
	
	@Field("meta_name")
	private String name;

	@Field("meta_score")
	private String score;
	
	@Field("meta_userscore")
	private String userscore;
	
	@Field("meta_developer")
    private String developer;
	
	@Field("meta_picture")
	private String picture;


	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getConsole() {
		return console;
	}

	public void setConsole(String console) {
		this.console = console;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getUserscore() {
		return userscore;
	}

	public void setUserscore(String userscore) {
		this.userscore = userscore;
	}

	public String getDeveloper() {
		return developer;
	}

	public void setDeveloper(String developer) {
		this.developer = developer;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}
	
	
}
