package es.udc.fic.riws.busquedaRIWS.searchEngine;

public class SearchEngineForm{
	
	private String console;
	
	private String name;
	
	private String minscore;
	
	private String maxscore;
	
	private String minuserscore;
	
	private String maxuserscore;

	private String developer;
	
	public String getConsole() {
		return console;
	}

	public void setConsole(String console) {
		this.console = console;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public String getMinscore() {
		return minscore;
	}

	public void setMinscore(String minscore) {
		this.minscore = minscore;
	}

	public String getMaxscore() {
		return maxscore;
	}

	public void setMaxscore(String maxscore) {
		this.maxscore = maxscore;
	}

	public String getMinuserscore() {
		return minuserscore;
	}

	public void setMinuserscore(String minuserscore) {
		this.minuserscore = minuserscore;
	}

	public String getMaxuserscore() {
		return maxuserscore;
	}

	public void setMaxuserscore(String maxuserscore) {
		this.maxuserscore = maxuserscore;
	}

	public String getDeveloper() {
		return developer;
	}

	public void setDeveloper(String developer) {
		this.developer = developer;
	}
		
}