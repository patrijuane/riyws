package es.udc.fic.riws.busquedaRIWS.searchEngine;

import java.io.IOException;
import java.util.List;

import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SearchEngineService{
	
	@Autowired
	private SearchEngineDao searchEngineDao;

	public List<VideoGame> videoGamesCrawled(String console, String name, String minscore, String maxscore,
			String minuserscore, String maxuserscore, String developer) throws SolrServerException, IOException{
		return searchEngineDao.queries(console, name, minscore, maxscore, minuserscore, maxuserscore, developer);
	}

	public List<String> consoles() throws SolrServerException, IOException{
		return searchEngineDao.consoles();
	}
	
}
